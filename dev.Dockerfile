# install node modules
FROM node:10.15.3 
WORKDIR /mnt/app
COPY ./hackernews /mnt/app
RUN yarn install


CMD [ "yarn", "dev"  ]