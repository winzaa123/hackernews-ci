# install node modules
FROM node:10.20.1-alpine  as install
WORKDIR /mnt/app
COPY ./hackernews /mnt/app
RUN yarn install

# build
FROM install as build

ENV NODE_ENV=production
WORKDIR /mnt/app
RUN yarn build

EXPOSE 3000

CMD [ "yarn", "start"  ]